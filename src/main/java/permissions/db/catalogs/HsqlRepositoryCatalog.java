package permissions.db.catalogs;

import java.sql.Connection;

import permissions.db.*;
import permissions.db.repos.*;

public class HsqlRepositoryCatalog implements RepositoryCatalog{

	Connection connection;
	
	public HsqlRepositoryCatalog(Connection connection) {
		this.connection = connection;
	}

	public PersonRepository people() {
		return new HsqlPersonRepository(connection);
	}
	public UserRepository users() {
		return new HsqlUserRepository(connection);
	}

	@Override
	public AddressRepository addresses() {
		return new HsqlAddressRepository(connection);
	}

	@Override
	public RoleRepository roles() {

		return new HsqlRoleRepository(connection);
	}

	@Override
	public PermissionRepository permission() {
	
		return new HsqlPermissionRepository(connection);
	}

}
