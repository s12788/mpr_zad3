package permissions.db;

import java.util.List;

import permissions.domain.*;

public interface AddressRepository extends Repository<Address> {
	
	public List<Address> withCountry(String country, PagingInfo page);
	public List<Address> withCity(String city, PagingInfo page);

}
