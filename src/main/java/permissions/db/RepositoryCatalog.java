package permissions.db;

public interface RepositoryCatalog {
	
	public PersonRepository people();
	public UserRepository users();
	public AddressRepository addresses();
	public RoleRepository roles();
	public PermissionRepository permission();
}
