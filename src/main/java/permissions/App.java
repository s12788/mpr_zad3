package permissions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import org.hsqldb.lib.HsqlArrayHeap;

import permissions.db.PagingInfo;
import permissions.db.PersonDbManager;
import permissions.db.RepositoryCatalog;
import permissions.db.catalogs.HsqlRepositoryCatalog;
import permissions.domain.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	
    	Person p1 = new Person();
    	p1.setName("Jan");
    	p1.setSurname("Kowalski");
    	
    	User u1 = new User();
    	u1.setUsername("jkowalski");
    	u1.setPassword("jan123");
    	
    	Address a1 = new Address();
    	a1.setCountry("Polska");
    	a1.setCity("Gdansk");
    	a1.setStreet("Brzegi");
    	a1.setPostalCode("80-001");
    	a1.setHouseNumber("22c");
    	a1.setLocalNumber("3");
    	a1.setId_person(1);
    	
    	Permission pr1 = new Permission();
    	pr1.setName("restricted");
    	
    	Role r1 = new Role();
    	r1.setName("local user");

    	String url = "jdbc:hsqldb:hsql://localhost/workdb";
		try(Connection connection = DriverManager.getConnection(url)) 
		{
		
	    	RepositoryCatalog catalogOf = new HsqlRepositoryCatalog(connection);  
	    	
	    	catalogOf.people().add(p1);
	    	catalogOf.users().add(u1);
	    	catalogOf.addresses().add(a1);
	    	catalogOf.permission().add(pr1);
	    	catalogOf.roles().add(r1);
	    	
	    	PagingInfo pagingInfo = new PagingInfo();
	    	pagingInfo.setSize(0);
	    	pagingInfo.setCurrentPage(0);
	    	pagingInfo.setTotalCount(0);
	    	
	    	
	        List<Person> allPersons = catalogOf.people().allOnPage(pagingInfo);        
	        for(Person p : allPersons){
	        	System.out.println(p.getId()+" | "+p.getName()+" | "+p.getSurname());
	        }
	        
	        System.out.println("");
	        
	        List<User> allUsers = catalogOf.users().allOnPage(pagingInfo);	        
	        for(User u : allUsers){
	        	System.out.println(u.getId()+" | "+u.getUsername()+" | "+u.getPassword());
	        }
	        
	        System.out.println("");
	        
	        List<Address> allAdddresses = catalogOf.addresses().allOnPage(pagingInfo);        
	        for(Address a : allAdddresses){
	        	System.out.println(a.getId()+" | "+a.getCountry()+" | "+a.getCity()+" "
	        			+a.getStreet()+" | "+a.getPostalCode()+" | "+a.getHouseNumber()
	        			+"/"+a.getLocalNumber()+" | "+a.getId_person());
	        }
	        
	        System.out.println("");
	        
	        List<Role> allRoles = catalogOf.roles().allOnPage(pagingInfo);	        
	        for(Role r : allRoles){
	        	System.out.println(r.getId()+" | "+r.getName());
	        }
	        
	        System.out.println("");
	        
	        List<Permission> allPermissions = catalogOf.permission().allOnPage(pagingInfo);	        
	        for(Permission p : allPermissions){
	        	System.out.println(p.getId()+" | "+p.getName());
	        }
	        
	        
	        
	        System.out.println("\n"+catalogOf.users().authenticate(u1));
	    	
	    	
	    	
	 
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
}
